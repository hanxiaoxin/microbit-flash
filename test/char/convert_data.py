import os
import re
import json

"""
https://led.baud-dance.com/ 字模
"""


pattern = r"/\*\s*([^*]+)\s\*/\s*\{([^}]+)\}"
# 获取当前工作目录
current_directory = os.getcwd()
# print(current_directory)

fonts = {}


def is_ascii(ch):
    # print(ch, str(ch))
    if len(ch) > 1:
        return False
    return ord(ch) < 128


def get_utf8_code(ch):
    if ch == "·":
        return ord(ch)

    data_code = ch.encode("utf-8")
    if is_ascii(data_code):
        idx = ord(ch)
        # print(idx)
        return idx
    else:
        code = 0x00
        code |= data_code[0] << 16  # 第一个字节
        code |= data_code[1] << 8  # 第二个字节
        code |= data_code[2]  # 第三个字节
        return code


def convert(line):
    # 使用正则表达式匹配注释和数字数组
    patternKey = r"/\*(.*?)\*/"
    matchKey = re.match(patternKey, line)
    key = None
    key = None
    # print(matchKey)
    if matchKey:
        key = matchKey.group().replace("/", "").replace("*", "")
        key = key[key.find(" ") + 1 :]
        key = key[key.find(" ") + 1 :]
        # print(key)

        if key != " ":
            key: int = get_utf8_code(key.replace(" ", ""))

    else:
        print("No key found")

    patternValue = r"\{.*\}"
    matchValue = re.search(patternValue, line)
    if matchValue:
        value = matchValue.group().replace("{", "").replace("}", "")
        # print(value)
    else:
        print("No value found")

    if key and value:
        arr = value.split(",")
        # 转换成数字
        v_arr = []
        for hex_str in arr:
            if hex_str:
                s = hex_str[2:]
                decimal_number = int(s, 16)
                v_arr.append(decimal_number)
        fonts[key] = v_arr
        # print(fonts)


def convert_file(font_size):
    global fonts
    fonts = {}
    input_file = open(f"{current_directory}/test/char/data_{font_size}.txt", "r")
    output_file = open(f"{current_directory}/test/char/fonts_{font_size}.py", "w")

    output_json_file = open(
        f"{current_directory}/test/char/fonts_{font_size}.json", "w"
    )

    # 逐行读取
    while True:
        line = input_file.readline()
        # 如果读取到文件末尾，break退出循环
        if not line:
            break
        # 打印每一行
        # print(line, end="")  # end='' 防止打印额外的空行
        convert(line)

    # 关闭文件
    input_file.close()
    # print(fonts)

    out_str = str(fonts).replace("'", "")
    output_file.write("font = " + out_str)
    output_file.close()
    json.dump(fonts, output_json_file)
    output_json_file.close()


convert_file(12)
convert_file(16)

def chunk_string(s, size):
    return [s[i : i + size] for i in range(0, len(s), size)]


# 示例使用
input_string = "这是一个示例字符串"
chunk_size = 3
chunks = chunk_string(input_string, chunk_size)
print(chunks)

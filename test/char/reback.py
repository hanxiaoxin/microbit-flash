# 原始字符串
a = r"\xe4\xb8\xad\xe6\x96\x87\xe6\xb5\x8b\xe8\xaf\x95\n"

# 创建字节对象
byte_array = bytes(a, "utf-8")

# 打印字节对象
print(byte_array)  # 输出：b'\xe4\xb8\xad\xe6\x96\x87\xe6\xb5\x8b\xe8\xaf\x95\n'

# 解码为字符串
decoded_string = byte_array.decode("utf-8")

print(decoded_string)  # 输出：中文测试

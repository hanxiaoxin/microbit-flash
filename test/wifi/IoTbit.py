from microbit import *
import sys


def contains_chinese(text):
    for char in text:
        if "\u4e00" <= char <= "\u9fff":
            return True
    return False


class IOT(object):
    def __init__(self):
        self.__table_id = "1"
        self.__ip = "dev-creation-hardware.codemao.cn"
        self.__port = "3000"
        self.initUart()

        self.__sendAT("AT+RESTORE", 1000)
        uart_str = ""
        timeOut = running_time()
        while True:
            if uart.any():
                uart_str = str(uart.read()) + uart_str
                if len(uart_str) > 60:
                    uart_str = ""
            elif running_time() - timeOut > 6000:
                break

        self.__sendAT("ATE0", 500)
        self.__sendAT("AT+CWMODE=1", 500)
        uart_str = ""
        timeOut = running_time()
        while True:
            if uart.any():
                uart_str = str(uart.read()) + uart_str
                if len(uart_str) > 60:
                    uart_str = ""
            elif running_time() - timeOut > 4000:
                break

    def initUart(self):
        uart.init(baudrate=115200, bits=8, parity=None, stop=1, tx=pin8, rx=pin12)

    def raiseError(self, msg):
        display.show(Image.SAD)
        sleep(1500)
        display.scroll(msg)
        display.show(Image.SAD)
        sys.exit()

    def __sendAT(self, command, wait=0):
        str = bytes(command + "\u000D\u000A", "utf-8")
        uart.write(str)
        sleep(wait)

    def __waitResponse(self):
        uart_str = ""
        time_out = running_time()
        while True:
            if uart.any():
                uart_str = str(uart.read()) + uart_str
                if "WIFI GOT IP" in uart_str:
                    return True
                elif "OK" in uart_str:
                    return True
                elif "CONNECT" in uart_str:
                    return True
                elif "CIPDOMAIN" in uart_str:
                    return True
                elif "CLOSED" in uart_str:
                    return False
                elif "FAIL" in uart_str:
                    return False
                elif "ERROR" in uart_str:
                    return False
                elif len(uart_str) > 60:
                    uart_str = " "
            elif running_time() - time_out > 4000:
                return False

    def __waitTCPResponse(self):
        uart_str = ""
        time_out = running_time()
        while True:
            if uart.any():
                line = str(uart.read())
                uart_str = line + uart_str
                with open("log.txt") as my_file:
                    my_file.write(line)
                if "Codemao_0" in uart_str:
                    return uart_str
                if "Codemao_1" in uart_str:
                    return uart_str
                elif "CLOSED" in uart_str:
                    return "CLOSED"
                elif "ERROR" in uart_str:
                    return "ERROR"
                elif len(uart_str) > 30:
                    uart_str = " "
            elif running_time() - time_out > 4000:
                return uart_str

    def connectWIFI(self, ssid, pw):
        ssid = str(ssid)
        pw = str(pw)
        self.__sendAT('AT+CWJAP="' + ssid + '","' + pw + '"', 3000)
        state = self.__waitResponse()
        if not state:
            return self.raiseError("WIFI ERROR")

        self.__sendAT('AT+CIPDOMAIN="www.baidu.com"', 2000)
        state = self.__waitResponse()
        if not state:
            return self.raiseError("DOMAIN ERROR")

        return True

    def connectBCMIOT(self, table_id):
        self.__table_id = table_id
        self.__sendAT('AT+CIPSTART="TCP","' + self.__ip + '",' + self.__port, 5000)
        state = self.__waitResponse()
        if not state:
            return self.raiseError("SERVER ERROR")
        return True

    def uploadBCMIOT(
        self,
        data1="",
        data2="",
        data3="",
        data4="",
        data5="",
        data6="",
        data7="",
        data8="",
    ):
        line = ",".join([str(data1), str(data2), str(data3), str(data4)])
        if contains_chinese(line):
            return self.raiseError("UPLOAD ERROR")
        payload = self.to_json_payload(
            {
                "op": "upload",
                "topic": self.__table_id,
                "name": "dataset",
                "data": line,
            }
        )
        return self.sendPayload(payload, "TCP")

    def BCMIOTSwitchOn(self):
        payload = self.to_json_payload(
            {
                "op": "query",
                "topic": self.__table_id,
                "name": "switch",
            }
        )
        res = self.sendPayload(payload, "TCP")

        t = type(res)

        if t == str:
            response = str(res)
            if "Codemao_1" in response:
                return True
            if "Codemao_0" in response:
                return False
        return False

    def disconnectBCMIOT(self):
        payload = self.to_json_payload({"op": "close"})
        return self.sendPayload(payload)

    def to_json_payload(self, data_dict):
        json_prefix = "{"
        json_suffix = "}"
        json_content = ""
        length = len(data_dict)
        count = 0

        for key in data_dict:
            count += 1
            k = '"' + key + '"'
            v = '"' + str(data_dict[key]) + '"'
            s = ":"
            unit = k + s + v
            if count < length:
                unit += ","
            json_content += unit
        return json_prefix + json_content + json_suffix

    def sendPayload(self, payload, type="AT"):
        self.__sendAT("AT+CIPSEND=" + str(len(payload) + 2), 500)
        if type == "AT":
            state = self.__waitResponse()
            if not state:
                return self.raiseError("UPLOAD ERROR")
            return state
        if type == "TCP":
            uart.read(200)
            self.__sendAT(payload, 500)
            state = self.__waitTCPResponse()
            if not state:
                return self.raiseError("TCP ERROR")
            return state


if __name__ == "__main__":
    display.show(Image.HAPPY)
    iot = IOT()
    iot.connectWIFI("Codemao-Office", "2<3]OzU(!h")
    display.show(1)
    iot.connectBCMIOT("Tiot_MAsz1ZRO")
    display.show(2)
    while True:
        if button_a.is_pressed():
            iot.uploadBCMIOT(1, 2, 3)
            display.scroll("upload")
        if button_b.is_pressed():
            res = iot.BCMIOTSwitchOn()
            display.scroll(res)
        if pin_logo.is_touched():
            sys.exit()

from microbit import *


class IOT(object):
    def __init__(self):
        self.__userToken = ""
        self.__topic = "1"
        self.ip = "106.53.186.168"
        self.port = "3000"
        self.init_uart()

        time_out = running_time()
        uart_str = ""
        while True:
            if uart.any():
                uart_str += str(uart.read())
                if len(uart_str) > 60:
                    uart_str = ""
            elif running_time() - time_out > 8000:
                break

    def init_uart(self):
        uart.init(baudrate=115200, bits=8, parity=None, stop=1, tx=pin8, rx=pin12)
        self.__sendAT("AT+RESTORE", 500)
        self.__sendAT("AT+RST", 1000)
        self.__sendAT("AT+CWMODE=1", 500)
        self.__sendAT("AT+CIPSNTPCFG=1,8,'ntp1.aliyun.com'", 100)

    def __sendAT(self, command, wait=0):
        cmd = bytes(command + "\u000D\u000A", "utf-8")
        uart.write(cmd)
        sleep(wait)

    def __wait_Response(self):
        uart_str = ""
        time_out = running_time()
        while True:
            if uart.any():
                uart_str = str(uart.read(), "UTF-8") + uart_str
                if "WIFI GOT IP" in uart_str:
                    return True
                elif "OK" in uart_str:
                    return True
                elif "FAIL" in uart_str:
                    return False
                elif "ERROR" in uart_str:
                    return False
                elif "CONNECT" in uart_str:
                    return True
                elif len(uart_str) > 60:
                    uart_str = ""
            elif running_time() - time_out > 8000:
                return False

    def connectWIFI(self, ssid, pw):
        self.__sendAT('AT+CWJAP="' + ssid + '","' + pw + '"', 500)
        return self.__waitResponse()

    def connectBCMIOT(self, userToken):
        self.__userToken = userToken
        self.__sendAT('AT+CIPSTART="TCP","' + self.ip + '",' + self.port, 5000)
        toSendStr = '{"token":"' + self.__userToken + '","op":"connect"}'
        self.__sendAT("AT+CIPSEND=" + str(len(toSendStr) + 2), 500)
        self.__sendAT(toSendStr, 100)
        return self.__waitResponse()

    def upload(self, topic, data):
        topic = str(topic)
        toSendStr = (
            '{"topic":"'
            + topic
            + '","token":"'
            + self.__userToken
            + '","op":"upload","data":"'
            + data
            + '"}'
        )
        self.__sendAT("AT+CIPSEND=" + str(len(toSendStr) + 2), 500)
        self.__sendAT(toSendStr, 100)
        display.scroll("TCP END")

    def disconnectBCMIOT(self):
        toSendStr = '{"op":"close"}'
        self.__sendAT("AT+CIPSEND=" + str(len(toSendStr) + 2), 500)
        self.__sendAT(toSendStr, 100)

    def BCMIOTSwitchOn(self):
        toSendStr = (
            '{"topic":"'
            + self.__topic
            + '","token":"'
            + self.__userToken
            + ('","op":"get", ' '"data":"switch"}')
        )
        self.__sendAT("AT+CIPSEND=" + str(len(toSendStr) + 2), 500)
        self.__sendAT(toSendStr, 100)
        return self.__waitResponse()

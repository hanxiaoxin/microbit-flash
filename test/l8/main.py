# 导入库
from microbit import *
from oled import *
from iotbit import *
from soilhumidity import *
from light import *
from neopixel import *

pingmu = OLED()
iot = IOT()
trshidu = SOILHUMIDITY(pin2)
guangmin = LIGHT(pin10)
dengdai = NeoPixel(pin13, 14)
display.show(0)

# 物联网相关
iot.connectWIFI("Codemao-Office", "2<3]OzU(!h")
display.show(1)
iot.connectBCMIOT("Tiot_dTW0MHvi")
display.show(2)

sleep(2000)

liangdu = 0
while True:
    guangqiang = guangmin.get_lightlevel()
    trshidu_pct = trshidu.get_soilhumidity()
    pingmu.set_center(trshidu_pct, guangqiang)
    if trshidu_pct < 60:
        pin16.write_digital(1)
        sleep(2000)
        pin16.write_digital(0)
    if guangqiang < 800:
        liangdu = liangdu + 20
    if guangqiang > 1000:
        liangdu = liangdu - 20
    if liangdu < 0:
        liangdu = 0
    if liangdu > 255:
        liangdu = 255
    shijian = iot.get_hour()
    display.scroll(shijian)
    if shijian > 18 or shijian < 6:
        liangdu = 0
    dengdai.fill((liangdu, liangdu, liangdu))
    dengdai.show()
    sleep(2000)

from microbit import *
from oled import *
from localtime import *


class MyException(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


def toStr(input):
    return str(input)


class IOT(object):
    def __init__(self, debug=False):
        self.__table_id = "1"
        self.__ip = "creation-hardware.codemao.cn"
        self.__port = "3000"
        self.oled = None
        self.wait = 500
        self.initUart()
        self.isUpload = False

        if debug:
            self.oled = OLED()
            self.oled.__command([0xD3, 0x00])
            self.oled.__set_zoom(0)
            self.oled.set_clear()

        self.__sendAT("AT+RESTORE", 1000)
        uart_str = ""
        timeOut = running_time()
        while True:
            if uart.any():
                line = str(uart.read())
                uart_str = line + uart_str
                self.debug_print(line)
                if "ERROR" in uart_str:
                    self.raiseError('START ERROR')
                if len(uart_str) > 60:
                    uart_str = ""
            elif running_time() - timeOut > 6000:
                break

        self.__sendAT("ATE0", 500)
        self.__sendAT("AT+CWMODE=1", 500)
        uart_str = ""
        timeOut = running_time()
        while True:
            if uart.any():
                line = str(uart.read())
                uart_str = line + uart_str
                self.debug_print(line)
                if len(uart_str) > 60:
                    uart_str = ""
            elif running_time() - timeOut > 4000:
                break

    def initUart(self):
        uart.init(baudrate=115200, bits=8, parity=None, stop=1, tx=pin8, rx=pin12)

    def scan(self):
        self.__sendAT("AT+CWLAPOPT=1,31", 200)
        self.__sendAT("AT+CWLAP", 500)
        uart_str = ""
        timeOut = running_time()
        while True:
            if uart.any():
                line = str(uart.read())
                uart_str = line + uart_str
                self.debug_print(line)
            elif running_time() - timeOut > 8000:
                break

    def raiseError(self, msg):
        display.show(Image.SAD)
        sleep(800)
        display.scroll(msg)
        display.show(Image.SAD)
        if msg == "TIME ERROR":
            reset_machine()
        if msg == "UPLOAD ERROR":
            reset_machine()
        if msg == "SERVER ERROR":
            reset_machine()
        if msg == "TCP ERROR":
            reset_machine()
        if msg == "DOMAIN ERROR":
            reset_machine()
        raise (MyException(msg))

    def __sendAT(self, command, wait=0):
        str = bytes(command + "\u000D\u000A", "utf-8")
        self.debug_print(str)
        uart.write(str)
        sleep(wait)

    def __waitResponse(self):
        uart_str = ""
        time_out = running_time()
        while True:
            if uart.any():
                line = str(uart.read())
                uart_str = line + uart_str
                self.debug_print(line)
                if "WIFI GOT IP" in uart_str:
                    return True
                elif "OK" in uart_str:
                    return True
                elif "CONNECT" in uart_str:
                    return True
                elif "CIPDOMAIN" in uart_str:
                    return True
                elif "CLOSED" in uart_str:
                    return False
                elif "FAIL" in uart_str:
                    return False
                elif "ERROR" in uart_str:
                    return False
                elif len(uart_str) > 60:
                    uart_str = " "
            elif running_time() - time_out > 4000:
                return False

    def __waitTCPResponse(self):
        uart_str = ""
        time_out = running_time()
        while True:
            if uart.any():
                line = str(uart.read())
                uart_str = line + uart_str
                self.debug_print(line)
                if "Codemao_0" in uart_str:
                    return uart_str
                if "Codemao_1" in uart_str:
                    return uart_str
                elif "CLOSED" in uart_str:
                    return "ERROR"
                elif "ERROR" in uart_str:
                    sleep(1000)
                    reset_machine()
                    return "ERROR"
                elif len(uart_str) > 30:
                    uart_str = ""
            elif running_time() - time_out > 4000:
                return uart_str

    def connectWIFI(self, ssid, pw):
        ssid = str(ssid)
        pw = str(pw)
        self.__sendAT('AT+CWJAP="' + ssid + '","' + pw + '"', 3000)
        state = self.__waitResponse()
        if not state:
            return self.raiseError("WIFI ERROR")

        self.__sendAT('AT+CIPDOMAIN="www.baidu.com"', 1000)
        state = self.__waitResponse()
        if not state:
            return self.raiseError("DOMAIN ERROR")
        return True

    def syncTime(self):
        if self.isUpload:
            return
        payload = self.to_json_payload(
            {
                "op": "query",
                "topic": self.__table_id,
                "name": "time",
            }
        )
        time_str = self.sendPayload(payload, "TCP")
        if "Codemao_1" in time_str:
            sync_net_time(time_str)
        if "ERROR" in time_str:
            return self.raiseError("TIME ERROR")

    def connectBCMIOT(self, table_id):
        self.__table_id = table_id
        self.__sendAT('AT+CIPSTART="TCP","' + self.__ip + '",' + self.__port, 3000)
        state = self.__waitResponse()
        if not state:
            return self.raiseError("SERVER ERROR")

        run_every(self.syncTime, s=30)

        self.syncTime()

        return True

    def uploadBCMIOT(
        self,
        data1="",
        data2="",
        data3="",
        data4="",
    ):
        self.isUpload = True
        line = ",".join([str(data1), str(data2), str(data3), str(data4)])
        payload = self.to_json_payload(
            {
                "op": "upload",
                "topic": self.__table_id,
                "name": "dataset",
                "data": line,
            }
        )
        resp = self.sendPayload(payload, "TCP")
        self.isUpload = False
        return resp

    def BCMIOTSwitchOn(self):
        payload = self.to_json_payload(
            {
                "op": "query",
                "topic": self.__table_id,
                "name": "switch",
            }
        )
        res = self.sendPayload(payload, "TCP")

        t = type(res)

        if t == str:
            response = str(res)
            if "Codemao_1" in response:
                return True
            if "Codemao_0" in response:
                return False
        return False

    def to_json_payload(self, data_dict):
        json_prefix = "{"
        json_suffix = "}"
        json_content = ""
        length = len(data_dict)
        count = 0

        for key in data_dict:
            count += 1
            k = '"' + key + '"'
            v = '"' + str(data_dict[key]) + '"'
            s = ":"
            unit = k + s + v
            if count < length:
                unit += ","
            json_content += unit
        return json_prefix + json_content + json_suffix

    def sendPayload(self, payload, type="AT"):
        self.__sendAT("AT+CIPSEND=" + str(len(payload) + 2), 500)
        if type == "AT":
            state = self.__waitResponse()
            if not state:
                return self.raiseError("UPLOAD ERROR")
            return state
        if type == "TCP":
            uart.read(200)
            self.__sendAT(payload, 500)
            state = self.__waitTCPResponse()
            if not state:
                return self.raiseError("TCP ERROR")
            return state

    def debug_print(self, text):
        if self.oled:
            self.oled.set_lines(str(text))
            sleep(self.wait)

    def get_year(self):
        return timer.year

    def get_month(self):
        return timer.month

    def get_day(self):
        return timer.day

    def get_hour(self):
        return timer.hour

    def get_minute(self):
        return timer.minute

    def get_second(self):
        return timer.second

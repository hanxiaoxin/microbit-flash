from microbit import *

class LocalTimer:
    def __init__(self, year, month, day, hour, minute, second):
        self.setTime(year, month, day, hour, minute, second)

    def setTime(self, year, month, day, hour, minute, second):
        self.year = int(year)
        self.month = int(month)
        self.day = int(day)
        self.hour = int(hour)
        self.minute = int(minute)
        self.second = int(second)

    def add_seconds(self, seconds):
        self.second += seconds
        if self.second >= 60:
            self.minute += self.second // 60
            self.second %= 60

        if self.minute >= 60:
            self.hour += self.minute // 60
            self.minute %= 60

        if self.hour >= 24:
            self.day += self.hour // 24
            self.hour %= 24

        while self.day > 31:
            self.month += 1
            self.day -= 31
            if self.month > 12:
                self.month = 1
                self.year += 1

    def get_time(self):
        d = str(self.year) + "-" + str(self.month) + "-" + str(self.day)
        t = str(self.hour) + ":" + str(self.minute) + ":" + str(self.second)
        return d + t


def addTime():
    timer.add_seconds(10)


def reset_machine():
    reset()


timer = LocalTimer(2024, 9, 6, 14, 11, 15)
run_every(addTime, s=10)


def sync_net_time(text):
    start = text.find("[")
    end = text.find("]", start)

    if start != -1 and end != -1:
        data = text[start + 1 : end]
        time_components = data.split(",")
        year, month, day, hour, minute, second = time_components
        timer.setTime(year, month, day, hour, minute, second)

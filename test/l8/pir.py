from microbit import *

class PIR(object):
    def __init__(self, pin):
        self.__pin = pin

    def detect(self) -> bool:
        if self.__pin.read_digital():
            return True
        else:
            return False

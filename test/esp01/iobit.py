from microbit import *

class Logger(object):
    def __init__(self) -> None:
        self.text = ''
        
    def add(self, v, prefix = ''):
        self.text = self.text + v + prefix
    
    def getLength(self):
        return str(len(self.text))
    
    def getLatest(self):
        r = self.text
        self.clear()
        return r
    
    def flush(self):
        display.scroll(len(self.text))
        uart.init(115200)
        r = self.text
        print(r)
        self.clear()
    
    def clear(self):
        self.text = ''


class IOT(object):
    """
        物联网
    """
    def __init__(self):
        self.logger = Logger()
        self.mqttRecv=""
        self.__userToken = ""
        self.__topic = "1"
        self.pin = False
    
        self.initUart()
        
        uart_str = ""
        timeOut = running_time()
        while True:
            # display.scroll('R')
            if uart.any():
                # display.scroll('W1')
                uart_str = str(uart.read()) + uart_str
                self.logger.add(uart_str)
                if len(uart_str) > 60:
                    uart_str = ""
            elif running_time() - timeOut > 4000:
                break
        display.scroll('OK:')
        display.scroll(self.logger.getLength())
        
        self.__sendAT("AT+CWMODE=1", 500)
        
        uart_str = ""
        timeOut = running_time()
        while True:
            # display.scroll('R')
            if uart.any():
                # display.scroll('W1')
                uart_str = str(uart.read()) + uart_str
                self.logger.add(uart_str)
                if len(uart_str) > 60:
                    uart_str = ""
            elif running_time() - timeOut > 8000:
                break
        display.scroll('OK:')
        display.scroll(self.logger.getLength())
    
    def initUart(self):
        # 已经初始化完毕不用重复初始化
        # display.scroll("PIN:")
        # display.scroll(self.pin)
        if self.pin:
            return
        # display.scroll('UART')
        uart.init(baudrate=115200, bits=8, parity=None, stop=1, tx=pin8, rx=pin12)
        # display.scroll("I")
        self.__sendAT("AT+RESTORE", 1000)
        # self.__sendAT("AT+RST", 1000)
        # self.__sendAT("AT+SYSTIMESTAMP=1634953609130", 100)
        # self.__sendAT("AT+CIPSNTPCFG=1,8,'ntp1.aliyun.com'", 100)
        display.scroll('END')
        self.pin = True

    def __sendAT(self, command: str, wait: int = 0):
        str = bytes(command + "\u000D\u000A", 'utf-8')
        uart.write(str)
        sleep(wait)

    def __waitResponse(self):
        uart_str = ""
        timeOut = running_time()
        while True:
            if uart.any():
                uart_str: str = str(uart.read(), 'UTF-8') + uart_str
                self.logger.add(uart_str)
                if "WIFI GOT IP" in uart_str:
                    display.scroll('IP')
                    return True
                elif "CONNECT" in uart_str:
                    display.scroll('CONNECT')
                    return True
                elif len(uart_str) > 60:
                    uart_str = ""
            elif running_time() - timeOut > 12000:
                display.scroll('OUT')
                return False

    def connectWIFI(self, ssid: str, pw: str):
        self.initUart()
        display.scroll("CCCC")
        self.__sendAT("AT+CWJAP=\"" + ssid + "\",\"" + pw + "\"", 500)
        return self.__waitResponse()

    def connectKidsiot(self, userToken:str, topic:str):
        self.__userToken = userToken
        self.__topic = topic
        display.scroll('KKKK:')
        self.__sendAT("AT+CIPSTART=\"TCP\",\"139.159.161.57\",5555", 5000)
        toSendStr = "{\"topic\":\"" + self.__topic + "\",\"userToken\":\"" + self.__userToken + "\",\"op\":\"init\"}"
        self.__sendAT("AT+CIPSEND="+str(len(toSendStr)+2), 500)
        self.__sendAT(toSendStr, 100)
        return self.__waitResponse()

    def uploadKidsiot(self,data:str):
        display.scroll('UUUU:')
        toSendStr = "{\"topic\":\"" + self.__topic + "\",\"userToken\":\"" + self.__userToken + "\",\"op\":\"up\",\"data\":\"" + data + "\"}"
        self.__sendAT("AT+CIPSEND="+str(len(toSendStr)+2), 500)
        self.__sendAT(toSendStr, 100)
        display.scroll('TCP END')

    def disconnectKidsiot(self):
        toSendStr = "{\"topic\":\"" + self.__topic + "\",\"userToken\":\"" + self.__userToken + "\",\"op\":\"close\"}"
        self.__sendAT("AT+CIPSEND="+str(len(toSendStr)+2), 500)
        self.__sendAT(toSendStr, 100)
        
    def flush(self):
        self.pin = False
        self.logger.flush()
from microbit import *

class Servo:
    """L8舵机
    """
    def __init__(self, pin):
        self.pin = pin

    def write_angle(self, degree):
        v_us = (degree * 2000 // 350) + 500
        duty_cycle = (v_us * 1024) // 20000
        self.pin.write_analog(duty_cycle)

if __name__ == "__main__":
    s = Servo(pin1)
    s.write_angle(90)
from microbit import *

cmd = [
    [0xAE],
    [0xA4],
    [0xD5, 0xF0],
    [0xA8, 0x3F],
    [0xD3, 0x03],
    [0 | 0x0],
    [0x8D, 0x14],
    [0x20, 0x00],
    [0x21, 0, 127],
    [0x22, 0, 63],
    [0xA0 | 0x1],
    [0xC8],
    [0xDA, 0x12],
    [0x81, 0xCF],
    [0xD9, 0xF1],
    [0xDB, 0x40],
    [0xA6],
    [0xD6, 0],
    [0xAF],
]

ADDR = 0x3C
screen = bytearray(1025)  # send byte plus pixels
screen[0] = 0x40
size_t = 10
chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ?.'


class OLED(object):
    """基本描述

    OLED1306显示屏

    """

    def __init__(self):
        for c in cmd:
            self.__command(c)

        self.set_clear()
        self.__set_zoom(0)
        self.il = 0
        self.last = ''

    def __command(self, c):
        i2c.write(ADDR, b"\x00" + bytearray(c))

    def __set_zoom(self, zoom=1):
        self.__command([0xD6, zoom])

    def __set_pos(self, col=0, page=0):
        self.__command([0xB0 | page])
        c = col
        c1, c2 = c & 0x0F, c >> 4
        self.__command([0x00 | c1])
        self.__command([0x10 | c2])

    def set_clear(self):
        global screen
        for i in range(1, 1025):
            screen[i] = 0
        self.set_refresh()

    def set_refresh(self):
        self.__set_pos()
        i2c.write(ADDR, screen)

    def set_text(self, x, y, s):
        global size_t
        ind = 0
        str_len = len(s)
        a1 = 0
        a2 = 0
        a3 = 0
        a4 = 0
        for i in range(0, str_len):
            ch = Image(s[i])
            # print(ch, i, x)
            for c in range(0, size_t):
                col = 0
                ci = int(c / 2)
                for r in range(1, 6):
                    p = ch.get_pixel(ci, r - 1)
                    col = col | (1 << r) if (p != 0) else col
                
                #print(bin(col))
                # 获取前四个像素
                a1 = col & 0b10000000
                a2 = col & 0b01000000
                a3 = col & 0b00100000
                a4 = col & 0b00010000
                
                #print(bin(a1), bin(a2), bin(a3), bin(a4))
                
                # 翻倍
                c1 = a1 | a1 >> 1
                c2 = a2 >> 1 | a2 >> 2
                c3 = a3 >> 2 | a3 >> 3
                c4 = a4 >> 3 | a4 >> 4
                
                #print(bin(c1), bin(c2), bin(c3), bin(c4))
                
                
                column1 = c1 | c2 | c3 | c4
                #print(bin(column1))
                #print("<<<<<<<<<<<<<<<<")
                
                # 获取后四个像素
                a5 = col & 0b00001000
                a6 = col & 0b00000100
                a7 = col & 0b00000010
                a8 = col & 0b00000001
                
                #print(bin(a5), bin(a6), bin(a7), bin(a8))
                # 翻倍
                c5 = a5 << 3 | a5 << 4
                c6 = a6 << 2 | a6 << 3
                c7 = a7 << 1 | a7 << 2
                c8 = a8 << 1
                
                #print(bin(c5), bin(c6), bin(c7), bin(c8))
                column2 = c5 | c6 | c7 | c8
                #print(bin(column2))
                #print("--------------------------")
                
                ind1 = int((x + i) * size_t + y * 128 + c)
                # print(ind)
                screen[ind1] = column2
                screen[ind1 + 1] = column2
                
                ind2 = int((x + i) * size_t + (y + 1) * 128 + c)
                screen[ind2] = column1
                screen[ind2 + 1] = column1
                
        left = int(x * size_t)
        top = int(y)
        self.__set_pos(left, top)
        ind0 = int(x * size_t + y * 128)
        i2c.write(ADDR, b"\x40" + screen[ind0: ind2 + 1])
        

    def set_number_center(self, text):
        global size_t
        [i, f] = text.split('.')

        int_length = len(i)

        if int_length < self.il:
            self.set_clear()

        self.il = int_length

        max_f = 3

        if len(f) < max_f:
            pad = max_f - len(f)
            for item in range(0, pad):
                f += '0'
        else:
            f = f[0:max_f]

        left = (120 - (len(i) + len(f)) * size_t) / 2 / size_t
        top = 3
        value = i + '.' + f
        #print(left, top)
        self.set_text(left, top, value)

    def set_text_center(self, text):
        left = (130 - len(text) * size_t) / 2 / size_t
        top = 3
        self.set_text(left, top, text)

    def set_center(self, text):
        global chars
        line = str(text)
        
        for ch in line:
            if ch not in chars:
                print("函数(set_center):不支持的字符 '", ch, "'")
                return

        if '?' in self.last:
            self.set_clear()

        self.last = line
        if '.' in line:
            self.set_number_center(line)
            return

        self.set_clear()
        self.set_text_center(line)


if __name__ == "__main__":
    pm = OLED()
    a = 0
    pm.set_center("Ready?")
    while True:
        # 按下a键开始计时，获取系统运行时间作为起点时间
        if button_a.was_pressed():
            a = running_time()

        # 实时显示当前时间与起点时间的差值
        if a > 0:
            b = running_time()
            sj = (b - a) / 1000
            pm.set_center(sj)

        # 按下b键结束计时，保持两秒钟后重置起点时间为0
        if button_b.was_pressed():
            b = running_time()
            sj = (b - a) / 1000
            pm.set_center(sj)
            sleep(2000)
            a = 0
            pm.set_center("Ready?")


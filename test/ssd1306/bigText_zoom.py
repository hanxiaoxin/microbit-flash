from microbit import *

# L8课程内置程序


def split_string_by_length(s, length):
    return [s[i : i + length] for i in range(0, len(s), length)]


cmd = [
    [0xAE],
    [0xA4],
    [0xD5, 0xF0],
    [0xA8, 0x3F],
    [0xD3, 0x03],
    [0 | 0x0],
    [0x8D, 0x14],
    [0x20, 0x00],
    [0x21, 0, 127],
    [0x22, 0, 63],
    [0xA0 | 0x1],
    [0xC8],
    [0xDA, 0x12],
    [0x81, 0xCF],
    [0xD9, 0xF1],
    [0xDB, 0x40],
    [0xA6],
    [0xD6, 0],
    [0xAF],
]

ADDR = 0x3C
screen = bytearray(1025)  # send byte plus pixels
screen[0] = 0x40
size_t = 10
chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ?.'


class OLED(object):
    """基本描述

    OLED1306显示屏

    """

    def __init__(self):
        for c in cmd:
            self.__command(c)

        self.set_clear()
        self.__set_zoom(0)
        self.il = 0
        self.last = ''

    def __command(self, c):
        i2c.write(ADDR, b"\x00" + bytearray(c))

    def __set_zoom(self, zoom=1):
        self.__command([0xD6, zoom])

    def __set_pos(self, col=0, page=0):
        self.__command([0xB0 | page])
        c = col
        c1, c2 = c & 0x0F, c >> 4
        self.__command([0x00 | c1])
        self.__command([0x10 | c2])

    def set_clear(self):
        global screen
        for i in range(1, 1025):
            screen[i] = 0
        self.set_refresh()

    def set_refresh(self):
        self.__set_pos()
        i2c.write(ADDR, screen)

    def set_text(self, x, y, s):
        global size_t
        ind = 0
        str_len = len(s)
        for i in range(0, str_len):
            ch = Image(s[i])
            # print(ch, i, x)
            for c in range(0, size_t):
                col = 0
                ci = int(c / 2)
                for r in range(1, 6):
                    p = ch.get_pixel(ci, r - 1)
                    col = col | (1 << r) if (p != 0) else col
                ind = int((x + i) * size_t + y * 128 + c)
                # print(ind)
                screen[ind] = col
                screen[ind + 1] = col
        left = int(x * size_t)
        top = int(y)
        self.__set_pos(left, top)
        ind0 = int(x * size_t + y * 128)
        i2c.write(ADDR, b"\x40" + screen[ind0: ind + 1])

    def set_number_center(self, text):
        global size_t
        [i, f] = text.split('.')

        int_length = len(i)

        if int_length < self.il:
            self.set_clear()

        self.il = int_length

        max_f = 3

        if len(f) < max_f:
            pad = max_f - len(f)
            for item in range(0, pad):
                f += '0'
        else:
            f = f[0:max_f]

        left = (120 - (len(i) + len(f)) * size_t) / 2 / size_t
        top = 3
        value = i + '.' + f
        # print(left, top)
        self.set_text(left, top, value)

    def set_text_center(self, text):
        left = (130 - len(text) * size_t) / 2 / size_t
        top = 2
        self.set_text(left, top, text)

    def set_center(self, *args):
        global chars
        line = " ".join(str(arg) for arg in args)

        for ch in line:
            if ch == " ":
                continue
            if ch not in chars:
                print("函数(set_center):不支持的字符 '", ch, "'")
                return

        if '?' in self.last:
            self.set_clear()

        self.last = line
        if '.' in line:
            self.set_number_center(line)
            return

        self.set_clear()
        self.set_text_center(line)

    def set_small_text(self, x, y, s):
        """

        显示一行文本

        Args:
            x (number): X 轴坐标偏移字符 1-24,屏幕120px,每个字符5px,最多24个字符
            y (number): Y 轴坐标偏移字符 0-63,每个5px
            s (str): 只接受字符串或字符类型参数

        Returns:
            NONE
        """
        for i in range(0, min(len(s), 12 * 2 - x)):
            char = Image(s[i])
            for c in range(0, 5):
                col = 0
                for r in range(1, 6):
                    p = char.get_pixel(
                        c, r - 1
                    )  # 遍历点阵字符，0表示空点，非0表示存在点
                    col = col | (1 << r) if (p != 0) else col
                ind = (x + i) * 5 + y * 128 + c
                screen[ind] = col
        self.__set_pos(x * 5, y)
        ind0 = x * 5 + y * 128
        i2c.write(ADDR, b"\x40" + screen[ind0 : ind + 1])

    def set_lines(self, input_string):
        lines = split_string_by_length(input_string, 24)
        for y in range(len(lines)):
            self.set_small_text(0, y, lines[y])


if __name__ == "__main__":
    pm = OLED()
    a = 0
    pm.set_center("ready?")
    while True:
        # 按下a键开始计时，获取系统运行时间作为起点时间
        if button_a.was_pressed():
            a = running_time()

        # 实时显示当前时间与起点时间的差值
        if a > 0:
            b = running_time()
            sj = (b - a) / 1000
            pm.set_center(sj)

        # 按下b键结束计时，保持两秒钟后重置起点时间为0
        if button_b.was_pressed():
            b = running_time()
            sj = (b - a) / 1000
            pm.set_center(sj)
            sleep(2000)
            a = 0
            pm.set_center("Ready?")

from microbit import *
from oled1306 import *
import random


oled = OLED1306() 

while True:
    oled.set_clear()
    sleep(500)
    v = round(random.random(), 3)
    oled.set_text(0,0,str(v))

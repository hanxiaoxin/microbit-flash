from microbit import *
import time

DEGREES = "°"


class DataError(Exception):
    0


class DHT11:
    def __init__(self, pin):
        self._pin = pin
        self._timestamp = 0
        self._temperature = 99.9
        self._humidity = 99.9

    def read(self):
        now = time.ticks_ms()
        if self._timestamp == 0 or now - self._timestamp > 2000:
            self._timestamp = now
            for i in range(10):
                try:
                    self._temperature, self._humidity = self._dht11_interact()
                    break
                except:
                    pass
        return self._temperature, self._humidity

    def get_temperature(self):
        data = self.read()
        return round(data[0],1)

    def get_humidity(self):
        data = self.read()
        return round(data[1],1)

    def _dht11_interact(self):
        pin = self._pin
        pin2bit = self._pin2bit()
        buffer_ = bytearray(256)
        length = len(buffer_) // 4 * 4
        for i in range(length, len(buffer_)):
            buffer_[i] = 1
        pin.write_digital(1)
        sleep(50)
        self._block_irq()
        pin.write_digital(0)
        sleep(20)
        pin.read_digital()
        pin.set_pull(pin.PULL_UP)
        if self._grab_bits(pin2bit, buffer_, length) != length:
            self._unblock_irq()
            raise Exception("Grab bits failed.")
        else:
            self._unblock_irq()
        data = self._parse_data(buffer_)
        del buffer_
        if data is None or len(data) != 40:
            if data is None:
                bits = 0
            else:
                bits = len(data)
            raise DataError("Too many or too few bits " + str(bits))
        data = self._calc_bytes(data)
        checksum = self._calc_checksum(data)
        if data[4] != checksum:
            raise DataError("Checksum invalid.")
        temp = data[2] + data[3] / 10
        humid = data[0] + data[1] / 10
        return temp, humid

    def _pin2bit(self):
        pin = self._pin
        if pin == pin0:
            shift = 2
        elif pin == pin1:
            shift = 3
        elif pin == pin2:
            shift = 4
        elif pin == pin3:
            shift = 15
        elif pin == pin4:
            shift = 19
        elif pin == pin6:
            shift = 21
        elif pin == pin7:
            shift = 22
        elif pin == pin8:
            shift = 10
        elif pin == pin9:
            shift = 9
        elif pin == pin10:
            shift = 30
        elif pin == pin12:
            shift = 12
        elif pin == pin13:
            shift = 17
        elif pin == pin14:
            shift = 1
        elif pin == pin15:
            shift = 13
        else:
            raise ValueError("function not suitable for this pin")
        return shift

    @staticmethod
    @micropython.asm_thumb
    def _block_irq():
        cpsid("i")

    @staticmethod
    @micropython.asm_thumb
    def _unblock_irq():
        cpsie("i")

    @staticmethod
    @micropython.asm_thumb
    def _grab_bits(r0, r1, r2):
        b(START)

        label(DELAY)
        mov(r7, 0xA7)
        label(delay_loop)
        sub(r7, 1)
        bne(delay_loop)
        bx(lr)

        label(READ_PIN)
        mov(r3, 0x50)
        lsl(r3, r3, 16)
        add(r3, 0x05)
        lsl(r3, r3, 8)
        add(r3, 0x10)
        ldr(r4, [r3, 0])
        mov(r3, 0x01)
        lsl(r3, r0)
        and_(r4, r3)
        lsr(r4, r0)
        bx(lr)

        label(START)
        mov(r5, 0x00)
        label(again)
        mov(r6, 0x00)
        bl(READ_PIN)
        orr(r6, r4)
        bl(DELAY)
        bl(READ_PIN)
        lsl(r4, r4, 8)
        orr(r6, r4)
        bl(DELAY)
        bl(READ_PIN)
        lsl(r4, r4, 16)
        orr(r6, r4)
        bl(DELAY)
        bl(READ_PIN)
        lsl(r4, r4, 24)
        orr(r6, r4)
        bl(DELAY)

        add(r1, r1, r5)
        str(r6, [r1, 0])
        sub(r1, r1, r5)
        add(r5, r5, 4)
        sub(r4, r2, r5)
        bne(again)

        label(RETURN)
        mov(r0, r5)

    @staticmethod
    def _parse_data(buffer_):
        D = 50
        E = bytearray(D)
        B = 0
        A = 0
        F = True
        for I in buffer_:
            if I == 1:
                B += 1
            elif A == 0 and B == 0:
                0
            elif F:
                B = 0
                F = False
            elif A >= D:
                0
            elif B > 0:
                E[A] = B
                B = 0
                A += 1
        if A == 0:
            return
        G = bytearray(A)
        for H in range(A):
            G[H] = E[H]
        return G

    @staticmethod
    def _calc_bytes(pull_up_lengths):
        G = range
        B = pull_up_lengths
        C = 1000
        F = 0
        for D in G(0, len(B)):
            E = B[D]
            if E < C:
                C = E
            if E > F:
                F = E
        J = C + (F - C) / 2
        H = bytearray(5)
        I = 0
        A = 0
        for D in G(len(B)):
            A = A << 1
            if B[D] > J:
                A = A | 1
            if (D + 1) % 8 == 0:
                H[I] = A
                I += 1
                A = 0
        return H

    @staticmethod
    def _calc_checksum(data):
        return data[0] + data[1] + data[2] + data[3] & 0xFF

from microbit import *
from oled import *


count = 0
txData = ""
rxData = ""
pm = OLED()
pm.__command([0xD3, 0x00])
pm.__set_zoom(0)
pm.set_clean()
pm.set_lines("start")


def readAll():
    uart_str = ""
    timeOut = running_time()
    while True:
        sleep(1000)
        # pm.set_lines('read:' + str(uart.any()) + str(running_time() - timeOut))
        if uart.any():
            line = str(uart.read())
            uart_str = line + uart_str
        if len(uart_str) > 60:
            uart_str = ""
        if running_time() - timeOut > 4000:
            break
    return uart_str


while True:
    if count % 2 == 0:
        uart.init()
        txData = readAll()
        if txData:
            pm.set_lines("cmd: " + txData)
            if "\r\n" in txData:
                count = count + 1
    if count % 2 == 1:
        uart.init(115200, tx=pin8, rx=pin12)
        if txData:
            uart.write(txData)
            rxData = readAll()
            pm.set_lines(str(rxData))
            count = count + 1
        else:
            count = count + 1
    display.show(count)

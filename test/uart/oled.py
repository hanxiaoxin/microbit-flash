from microbit import *


def split_string_by_length(s, length):
    return [s[i : i + length] for i in range(0, len(s), length)]


cmd = [
    [0xAE],
    [0xA4],
    [0xD5, 0xF0],
    [0xA8, 0x3F],
    [0xD3, 0x03],
    [0 | 0x0],
    [0x8D, 0x14],
    [0x20, 0x00],
    [0x21, 0, 127],
    [0x22, 0, 63],
    [0xA0 | 0x1],
    [0xC8],
    [0xDA, 0x12],
    [0x81, 0xCF],
    [0xD9, 0xF1],
    [0xDB, 0x40],
    [0xA6],
    [0xD6, 0],
    [0xAF],
]

ADDR = 0x3C
screen = bytearray(1025)
screen[0] = 0x40
size_t = 10


class OLED(object):
    """基本描述

    OLED1306显示屏

    """

    def __init__(self):
        for c in cmd:
            self.__command(c)

        self.set_clear()
        self.__set_zoom(1)
        self.il = 0
        self.last = ""
        self.debug_line = 0

    def __command(self, c):
        i2c.write(ADDR, b"\x00" + bytearray(c))

    def __set_zoom(self, zoom=1):
        self.__command([0xD6, zoom])

    def __set_pos(self, col=0, page=0):
        self.__command([0xB0 | page])
        c = col
        c1, c2 = c & 0x0F, c >> 4
        self.__command([0x00 | c1])
        self.__command([0x10 | c2])

    def set_clear(self):
        global screen
        for i in range(1, 1025):
            screen[i] = 0
        self.set_refresh()

    def set_refresh(self):
        self.__set_pos()
        i2c.write(ADDR, screen)

    def set_text(self, x, y, s):
        global size_t
        ind = 0
        str_len = len(s)
        for i in range(0, str_len):
            ch = Image(s[i])
            # print(ch, i, x)
            for c in range(0, size_t):
                col = 0
                ci = int(c / 2)
                for r in range(1, 6):
                    p = ch.get_pixel(ci, r - 1)
                    col = col | (1 << r) if (p != 0) else col
                ind = int((x + i) * size_t + y * 128 + c)
                # print(ind)
                screen[ind] = col
                screen[ind + 1] = col
        left = int(x * size_t)
        top = int(y)
        self.__set_pos(left, top)
        ind0 = int(x * size_t + y * 128)
        i2c.write(ADDR, b"\x40" + screen[ind0 : ind + 1])

    def set_number_center(self, text):
        global size_t
        values = text.split(".")
        [i, f] = values
        int_length = len(i)

        if int_length < self.il:
            self.set_clear()

        self.il = int_length

        max_f = 3

        if len(f) < max_f:
            pad = max_f - len(f)
            for item in range(0, pad):
                f += "0"
        else:
            f = f[0:max_f]

        left = (120 - (len(i) + len(f)) * size_t) / 2 / size_t
        top = 2
        value = i + "." + f
        self.set_text(left, top, value)

    def set_numbers_center(self, line):
        nums = line.split(" ")
        num1 = ""
        num2 = ""
        num3 = ""
        if len(nums) == 1:
            num1 = nums[0]
        if len(nums) == 2:
            num1 = nums[0]
            num2 = nums[1]
            num2 = str(int(num2.split(".")[0]))
        if len(nums) == 3:
            num1 = nums[0]
            num2 = nums[1]
            num3 = nums[2]
            num2 = str(int(num2.split(".")[0]))
            num3 = str(int(num3.split(".")[0]))
        if len(num1) > 4:
            num1 = "99.9"
        text = num1 + " " + num2 + " " + num3
        self.set_text_center(text)

    def set_text_center(self, text):
        left = (130 - len(text) * size_t) / 2 / size_t
        top = 2
        self.set_text(left, top, text)

    def set_center(self, *args):
        global chars
        line = " ".join(str(arg) for arg in args)

        for ch in line:
            if ch == " ":
                continue
        if "?" in self.last:
            self.set_clear()

        self.last = line
        c = line.count(".")
        if c == 1:
            self.set_number_center(line)
            return
        if c > 1:
            self.set_clear()
            self.set_numbers_center(line)
            return
        self.set_clear()
        self.set_text_center(line)

    def set_small_text(self, x, y, s):
        for i in range(0, min(len(s), 12 * 2 - x)):
            for c in range(0, 5):
                col = 0
                for r in range(1, 6):
                    p = Image(s[i]).get_pixel(c, r - 1)
                    col = col | (1 << r) if (p != 0) else col
                ind = (x + i) * 5 + y * 128 + c
                screen[ind] = col
        self.__set_pos(x * 5, y)
        ind0 = x * 5 + y * 128
        i2c.write(ADDR, b"\x40" + screen[ind0 : ind + 1])

    def set_lines(self, input_string):
        lines = split_string_by_length(str(input_string), 24)
        for y in range(len(lines)):
            if self.debug_line > 7:
                self.debug_line = 0
                self.set_clean()
            self.set_small_text(0, self.debug_line, lines[y])
            self.debug_line += 1

    def set_clean(self):
        data = bytearray(1025)
        data[0] = 0x40
        for i in range(1024):
            data[i + 1] = 0
        i2c.write(ADDR, data)

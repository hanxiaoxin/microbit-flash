from microbit import *
import neopixel
import time

#级联数量
_n=5

#初始化
np = neopixel.NeoPixel(pin2, _n)

'''
@功能:点亮
参数:颜色
'''
def Light_it(color_r,color_g,color_b):
    # Draw a red gradient.
    # (R,G,B)
    for i in range(_n):
        np[i] = (color_r, color_g, color_b) 
    np.show()
    
def Fill_it():
    np.fill((100,100,0))
    np.show()

'''
@功能:清空颜色
'''
def Light_clear():
    for i in range(0,_n):
        np[i]=(0,0,0)
    np.clear()
'''
主函数
'''
def main():
    #红色
    Light_clear()
    Light_it(100,0,0)
    #延时(秒)
    time.sleep(2)
    #绿色
    Light_clear()
    Light_it(0,100,0)
    #延时
    time.sleep(2)
    #蓝色
    Light_clear()
    Light_it(0,0,100)
    #延时
    time.sleep(2)
    

Fill_it()
    
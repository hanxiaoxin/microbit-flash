### env

> nodejs >= 16

### 运行测试

自动下载workspace文件夹下面的python模块，文件夹目前只支持python文件，以及不能嵌套文件夹。

<pre>
    npm run test
</pre>

### REPL

目前没实现python读取串口数据，之后可能会加入, 同时vscode自带serial插件，可以先使用。
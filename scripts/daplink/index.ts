import { WebUSB } from "usb";
import { MicrobitWebUSBConnection } from "../../src/device/webusb";
import { DAPInfoRequest } from '../../src/lib/dapjs/index';

const logging = {
    event: console.log,
    error: console.log,
    log: console.log,
};

type REQ = keyof typeof DAPInfoRequest;

type DAP_REQUEST = Record<REQ, number>;

const dapReq: Partial<DAP_REQUEST> = {
    VENDOR_ID: 1,
    PRODUCT_ID: 2,
    SERIAL_NUMBER: 3,
    CMSIS_DAP_FW_VERSION: 4,
    TARGET_DEVICE_VENDOR: 5,
    TARGET_DEVICE_NAME: 6
};

/**
 * flash default demo file
 */
async function testDefault() {
    const customWebUSB = new WebUSB({
        devicesFound: async (devices) =>
            devices.find((device) => device.vendorId === 0x0d28),
    });
    const webusbConnection = new MicrobitWebUSBConnection({ logging });
    webusbConnection.getOneAvailableDevice = async () => {
        const device = await customWebUSB.requestDevice({
            filters: [],
        });
        return device;
    };
    await webusbConnection.connect();
    const daplink = webusbConnection.connection?.daplink;

    // console.log(daplink);

    if (!daplink) {
        console.error('daplink connection failed');
        return;
    }

    const venderID = await daplink.dapInfo(dapReq.VENDOR_ID || 1);
    const productID = await daplink.dapInfo(dapReq.PRODUCT_ID || 1);
    const serialNumber = await daplink.dapInfo(dapReq.SERIAL_NUMBER || 1);
    const version = await daplink.dapInfo(dapReq.CMSIS_DAP_FW_VERSION || 1);
    const device_vender = await daplink.dapInfo(dapReq.TARGET_DEVICE_VENDOR || 1);
    const device_name = await daplink.dapInfo(dapReq.TARGET_DEVICE_NAME || 1);
    console.log('---------------daplink info------------');
    console.log('vender ID: ', venderID);
    console.log('product ID: ', productID);
    console.log('serialNumber: ', serialNumber);
    console.log('version: ', version);
    console.log('device vender: ', device_vender);
    console.log('device name: ', device_name);

    console.log('CODEPAGESIZE: ', webusbConnection.connection?._pageSize);
    console.log('CODESIZE: ', webusbConnection.connection?._numPages)
    console.log('---------------daplink end------------');
    await webusbConnection.connection?.cortexM.disconnect();
    // await webusbConnection.connection?.daplink.disconnect();
    await webusbConnection.disconnect();
    return;
}

testDefault();
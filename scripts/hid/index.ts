import hid from 'node-hid';
import { HID, DAPLink } from '../../src/lib/dapjs';

async function init() {
    let devices = hid.devices();
    devices = devices.filter(device => device.vendorId === 0xD28);

    if (devices[0] && devices[0].path) {
        console.log(devices[0]);
        const device = new hid.HID(devices[0].path);
        const transport = new HID(device);
        const daplink = new DAPLink(transport);

        try {
            await daplink.connect();
            console.log('success');
            for (let i = 0; i < 100; i++) {
                daplink.serialWrite('17');
            }
            await daplink.disconnect();
        } catch (error) {
            console.error(error.message || error);
        }
    } else {
        console.error('no device found');
    }
}

init();
import fs from 'fs';
import path from 'path';

const savePath = path.resolve(__dirname, './workspace.hex');

export function saveHexFile(hexStr: string) {
    fs.writeFileSync(savePath, hexStr);
    console.log('save hex end: ', savePath)
}
import { flashWorkspace } from './webusb-test';
import { program } from 'commander';
import path from 'path';
import fs from 'fs';

program
  .option('-d,--dir <type>', 'specific working directory');

const defaultDir = path.resolve(__dirname, '../../', 'workspace');

program.parse(process.argv);

const options = program.opts();
const workDir = options.dir ?  options.dir : defaultDir;

if(!fs.existsSync(workDir)) {
    throw new Error('workDir is not exist!');
}

flashWorkspace(workDir);
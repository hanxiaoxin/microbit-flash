import fs from 'fs';
import path from 'path';

interface IPythonModule {
    filename: string;
    content: string;
}

/**
 * 
 * @returns 
 */
export function readWorkspaceFiles(workspaceDir: string) {
    const files = fs.readdirSync(workspaceDir);

    const modules: IPythonModule[] = files.map(fileName => {
        const filePath = path.resolve(workspaceDir, fileName);
        console.log(filePath);
        return {
            filename: fileName,
            content: fs.readFileSync(filePath, 'utf-8')
        }
    });

    console.log('collect modules: ', modules.map(module => module.filename));

    return modules;
}
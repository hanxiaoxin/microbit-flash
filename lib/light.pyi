from .microbit import MicroBitAnalogDigitalPin, MicroBitDigitalPin, MicroBitTouchPin

class LIGHT:
    def __init__(
        self, pin: MicroBitAnalogDigitalPin | MicroBitDigitalPin | MicroBitTouchPin
    ) -> None: ...
    @classmethod
    def get_lightlevel(self) -> int: ...

from microbit import MicroBitDigitalPin
from typing import Any

def toStr(input: Any):
    return str(input)

class IOT:
    def __init__(self, debug=False) -> IOT: ...
    @classmethod
    def __sendAT(self, cmd: str, time: int) -> None: ...
    @classmethod
    def connectWIFI(self, ssid: str, password: str) -> bool: ...
    @classmethod
    def connectBCMIOT(self, topic: str | int) -> bool: ...
    @classmethod
    def uploadBCMIOT(self, data1, data2=None, data3=None, data4=None) -> str: ...
    @classmethod
    def BCMIOTSwitchOn(self) -> bool: ...
    @classmethod
    def disconnectBCMIOT(self) -> bool: ...
    @classmethod
    def get_year(self) -> int: ...
    @classmethod
    def get_month(self) -> int: ...
    @classmethod
    def get_day(self) -> int: ...
    @classmethod
    def get_hour(self) -> int: ...
    @classmethod
    def get_minute(self) -> int: ...
    @classmethod
    def get_second(self) -> int: ...

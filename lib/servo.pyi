from .microbit import MicroBitAnalogDigitalPin, MicroBitDigitalPin, MicroBitTouchPin

class Servo:
    def __init__(
        self, pin: MicroBitAnalogDigitalPin | MicroBitDigitalPin | MicroBitTouchPin
    ) -> Servo: ...
    @classmethod
    def write_angle(self, angle: int) -> None: ...

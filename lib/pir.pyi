from .microbit import MicroBitAnalogDigitalPin, MicroBitDigitalPin, MicroBitTouchPin

class PIR:
    def __init__(
        self, pin: MicroBitAnalogDigitalPin | MicroBitDigitalPin | MicroBitTouchPin
    ) -> None: ...
    @classmethod
    def detect(self) -> bool: ...

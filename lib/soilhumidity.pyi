from .microbit import MicroBitAnalogDigitalPin, MicroBitDigitalPin, MicroBitTouchPin

class SOILHUMIDITY:
    def __init__(
        self, pin: MicroBitAnalogDigitalPin | MicroBitDigitalPin | MicroBitTouchPin
    ) -> None: ...
    @classmethod
    def get_soilhumidity(self) -> float: ...

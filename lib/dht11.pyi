from .microbit import MicroBitAnalogDigitalPin, MicroBitDigitalPin, MicroBitTouchPin

class DHT11:
    def __init__(
        self, pin: MicroBitAnalogDigitalPin | MicroBitDigitalPin | MicroBitTouchPin
    ) -> None: ...
    @classmethod
    def get_temperature(self) -> float: ...
    @classmethod
    def get_humidity(self) -> float: ...

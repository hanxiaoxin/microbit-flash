from microbit import *

class LIGHT(object):
    def __init__(self, pin):
        self.__pin = pin

    def get_lightlevel(self):
        raw_value = 0
        for i in range(10):
            raw_value += self.__pin.read_analog()
            sleep(1)
        raw_value /= 10
        if raw_value <= 960:
            lux = (raw_value - 7) * (550 / (960 - 7))
        elif raw_value <= 980:
            lux = 550 + (raw_value - 960) * ((2000 - 550) / (980 - 960))
        else:
            lux = 2000 + (raw_value - 980) * ((16000 - 2000) / (1005 - 980))

        return int(max(0, lux)) 
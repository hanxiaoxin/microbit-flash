from microbit import *

class SOILHUMIDITY(object):
    def __init__(self, pin) -> None:
        self.__pin = pin

    def get_soilhumidity(self):
        __value = self.__pin.read_analog()
        value = ((__value - 0) * (100 - 0)) / (1023 - 0)
        return int(value)

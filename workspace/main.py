from microbit import *
from oled import *
from iotbit import *
from soilhumidity import *
from light import *
from neopixel import *

pingmu = OLED()
dengdai = NeoPixel(pin16, 8)  # 灯带接数字引脚16
liangdu = 10

dengdai.fill((liangdu, liangdu, liangdu))
dengdai.show()

iot = IOT(False)
guangmin = LIGHT(pin1)  # 亮度传感器接模拟引脚1
trshidu = SOILHUMIDITY(pin2) #土壤湿度传感器接模拟引脚2
display.show(0)

# # 物联网相关
iot.connectWIFI("Codemao-Office", "2<3]OzU(!h")
display.show(1)
iot.connectBCMIOT("Tiot_dTW0MHvi")
display.show(2)

sleep(2000)

while True:
    guangqiang = guangmin.get_lightlevel()
    trshidu_pct = trshidu.get_soilhumidity()
    if guangqiang < 800:
        liangdu = liangdu + 20
    if guangqiang > 1000:
        liangdu = liangdu - 20
    if liangdu < 0:
        liangdu = 0
    if liangdu > 255:
        liangdu = 255
    shijian = iot.get_hour()
    pingmu.set_center(guangqiang, shijian, liangdu)
    if shijian > 18 or shijian < 6:
        liangdu = 0
    dengdai.fill((liangdu, liangdu, liangdu))
    dengdai.show()
    sleep(2000)

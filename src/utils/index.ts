import { microbitBoardId } from "@microbit/microbit-fs";
import v1 from "./microbit-micropython-v1";
import v2 from "./microbit-micropython-v2";
import { MicroPythonSource } from "../micropython/micropython";
import { Logging } from "../logging/logging";
import { DefaultHost } from "../fs/host";
import { FileSystem, VersionAction } from "../fs/fs";

export const fsMicroPythonSource: MicroPythonSource = async () => {
  return [
    {
      boardId: microbitBoardId.V1,
      hex: v1,
    },
    {
      boardId: microbitBoardId.V2,
      hex: v2,
    },
  ];
};

export async function generateMicrobitFS(
  logging: Logging,
  files: { name: string; content: string }[]
) {
  const host = new DefaultHost();
  const ufs = new FileSystem(logging, host, fsMicroPythonSource);
  for (const file of files) {
    await ufs.write(file.name, file.content, VersionAction.MAINTAIN);
  }
  return ufs;
}

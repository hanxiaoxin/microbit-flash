import { BoardId } from "./board-id";

/**
 * 主板序列号信息，分三部分
 * 主板ID 0-4 v1v2
 * familyID 4-8 产品线
 * 控制器ID 8 - 48 人机交互USB ID？
 * 9906 3602 0005282084a42e951e689052000000006e052820
 */
export class BoardSerialInfo {
  constructor(
    public id: BoardId,
    public familyId: string,
    public hic: string
  ) {}
  static parse(device: USBDevice, log: (msg: string) => void) {
    const serial = device.serialNumber;
    if (!serial) {
      throw new Error("Could not detected ID from connected board.");
    }
    if (serial.length !== 48) {
      log(`USB serial number unexpected length: ${serial.length}`);
    }
    const id = serial.substring(0, 4);
    const familyId = serial.substring(4, 8);
    const hic = serial.slice(-8);
    return new BoardSerialInfo(BoardId.parse(id), familyId, hic);
  }

  eq(other: BoardSerialInfo) {
    return (
      other.id === this.id &&
      other.familyId === this.familyId &&
      other.hic === this.hic
    );
  }
}

// https://github.com/mmoskal/dapjs/blob/a32f11f54e9e76a9c61896ddd425c1cb1a29c143/src/dap/constants.ts
// https://github.com/mmoskal/dapjs/blob/a32f11f54e9e76a9c61896ddd425c1cb1a29c143/src/cortex/constants.ts

// CRA's build tooling doesn't support const enums so we've converted them to prefixed constants here.
// If we move this to a separate library then we can replace them.
// In the meantime we should prune the list below to what we actually use.

// FICR Registers Fast Information Configuration Registers
export const FICR = {
  CODEPAGESIZE: 0x10000000 | 0x10, // 表示固件配置寄存器（FICR）中的代码页大小
  CODESIZE: 0x10000000 | 0x14,  // 表示FICR中每页的大小
};

export const DapCmd = {
  DAP_INFO: 0x00,  // 获取DAP接口信息的命令
  DAP_CONNECT: 0x02, // 连接DAP接口的命令
  DAP_DISCONNECT: 0x03, // 断开DAP接口连接的命令
  DAP_TRANSFER: 0x05, // 执行单个数据传输的命令
  DAP_TRANSFER_BLOCK: 0x06, // 执行数据块传输的命令
  // Many more.
};

export const Csw = {
  CSW_SIZE: 0x00000007,  // 通信状态寄存器（CSW）的大小字段
  CSW_SIZE32: 0x00000002, // 表示数据块大小为32字节的标志
  CSW_ADDRINC: 0x00000030, // 地址递增标志
  CSW_SADDRINC: 0x00000010, // 源地址递增标志
  CSW_DBGSTAT: 0x00000040, // 调试状态标志
  CSW_HPROT: 0x02000000, // 高优先级传输保护
  CSW_MSTRDBG: 0x20000000, // 主设备调试状态
  CSW_RESERVED: 0x01000000, // 保留位
  CSW_VALUE: -1, // see below
  // Many more.
};
Csw.CSW_VALUE =
  Csw.CSW_RESERVED |
  Csw.CSW_MSTRDBG |
  Csw.CSW_HPROT |
  Csw.CSW_DBGSTAT |
  Csw.CSW_SADDRINC; // 组合了多个标志

export const DapVal = {
  AP_ACC: 1 << 0, // 访问端口（AP）访问标志
  READ: 1 << 1, // 读操作标志
  WRITE: 0 << 1, // 写操作标志
  // More.
};

export const ApReg = {
  CSW: 0x00, // 通信状态寄存器（CSW）的地址
  TAR: 0x04, // 目标地址寄存器（TAR）的地址
  DRW: 0x0c, // 数据寄存器（DRW）的地址
  // More.
};

export const CortexSpecialReg = {
  // Debug Exception and Monitor Control Register 
  DEMCR: 0xe000edfc, // 调试异常和监视控制寄存器（DEMCR）的地址
  // DWTENA in armv6 architecture reference manual
  DEMCR_VC_CORERESET: 1 << 0, // 导致核心复位的标志

  // CPUID Register
  CPUID: 0xe000ed00, // CPU ID寄存器的地址

  // Debug Halting Control and Status Register
  DHCSR: 0xe000edf0, // 调试停止控制和状态寄存器（DHCSR）的地址
  S_RESET_ST: 1 << 25, // 系统复位标志

  NVIC_AIRCR: 0xe000ed0c, // 中断向量表偏移寄存器（NVIC AIRCR）的地址
  NVIC_AIRCR_VECTKEY: 0x5fa << 16, // 向NVIC AIRCR写入时需要的密钥值
  NVIC_AIRCR_SYSRESETREQ: 1 << 2, // 系统复位请求标志

  // Many more.
};

/**
 * 计算串口8bit数据的行数
 * 因为串口是通过LF,CR换行的
 * @param arr 
 * @returns 
 */
export const lineNumFromUint8Array = (arr: Uint8Array): number => {
  let lineCount = 1;
  let prevByte: number | undefined;
  const LF = 10;
  const CR = 13;
  arr.forEach((byte) => {
    if ((byte === LF && prevByte !== CR) || byte === CR) {
      lineCount++;
    }
    prevByte = byte;
  });
  return lineCount;
};

import { IntelHexWithId } from "@microbit/microbit-fs";

export type MicroPythonSource = () => Promise<IntelHexWithId[]>;
